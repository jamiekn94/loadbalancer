﻿using System;

namespace Loadbalancer.Health
{
    class MonitorResult
    {
        public readonly bool IsOnline;
        public readonly DateTime? DownDate;
        public readonly DateTime OnlineDate;
        public readonly int Attempts;

        public MonitorResult(int attempts, DateTime? downDate, bool isOnline)
        {
            Attempts = attempts;
            IsOnline = isOnline;
            DownDate = downDate;
            OnlineDate = DateTime.Now;
        }

        public int GetAmountSecondsOffline()
        {
            if (!DownDate.HasValue)
            {
                return 0;
            }

            return (int) Math.Round((OnlineDate - DownDate.Value).TotalSeconds, 0);
        }
    }
}