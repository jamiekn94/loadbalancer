﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Loadbalancer.Health
{
    class Monitor
    {
        public readonly WebServer WebServer;
        public readonly Uri Url;

        private readonly int _maxAttempts;
        private readonly int _timeOut;

        private DateTime? _dateDown;
        private int _failedConnectAttempts;

        public Monitor(WebServer webServer, int maxAttempts, int timeOut)
        {
            WebServer = webServer;
            _failedConnectAttempts = 0;
            _maxAttempts = maxAttempts;
            _timeOut = timeOut;

            Url = new UriBuilder(GetUriScheme(), WebServer.Host, WebServer.Port).Uri;
        }

        public async Task<MonitorResult> CheckAsync()
        {
            bool isOkStatusCode = true;
            int monitorResultFailedConnectAttempts;
            DateTime? monitorResultDateDown = null;

            try
            {
                var response = await DoRequestAsync();

                isOkStatusCode = ((HttpWebResponse) response).StatusCode == HttpStatusCode.OK;

                response.Dispose();
            }
            catch (WebException)
            {
                isOkStatusCode = false;
            }
            finally
            {
                if (isOkStatusCode)
                {
                    monitorResultDateDown = _dateDown;
                    monitorResultFailedConnectAttempts = _failedConnectAttempts + 1;

                    _dateDown = null;
                    _failedConnectAttempts = 0;
                }
                else
                {
                    if (_failedConnectAttempts == 0)
                    {
                        _dateDown = DateTime.Now;

                        monitorResultDateDown = _dateDown;
                    }

                    _failedConnectAttempts++;

                    monitorResultFailedConnectAttempts = _failedConnectAttempts;
                }
            }

            return new MonitorResult(monitorResultFailedConnectAttempts, monitorResultDateDown, isOkStatusCode);
        }

        private string GetUriScheme()
        {
            return WebServer.IsSecure ? Uri.UriSchemeHttps : Uri.UriSchemeHttp;
        }

        private async Task<WebResponse> DoRequestAsync()
        {
            WebRequest request = WebRequest.Create(Url);
            request.Method = "HEAD";
            request.Timeout = _timeOut;

            return await request.GetResponseAsync();
        }

        public bool IsOnline()
        {
            return _maxAttempts > _failedConnectAttempts;
        }
    }
}