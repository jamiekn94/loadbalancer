﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace Loadbalancer.Health
{
    class HealthMonitor
    {
        public delegate void ServerCheck(Monitor monitor);
        public delegate void ServerDown(Monitor monitor, MonitorResult monitorResult);
        public delegate void ServerUp(Monitor monitor, MonitorResult monitorResult);

        public event ServerCheck ServerCheckHandler;
        public event ServerDown ServerDownHandler;
        public event ServerDown ServerUpHandler;

        private const int MAX_ATTEMPTS = 3;
        private const int TIME_OUT = 5000;

        private readonly List<Monitor> _monitors;

        public HealthMonitor()
        {
            _monitors = new List<Monitor>();
        }

        public void Start()
        {
            var timer = new Timer
            {
                Interval = 5000
            };

            timer.Elapsed += async (sender, e) =>
            {
                TriggerServersCheck();

                await CheckServers();
            };

            timer.Start();
        }

        public void AddServer(WebServer webServer)
        {
            _monitors.Add(new Monitor(webServer, MAX_ATTEMPTS, TIME_OUT));
        }

        public List<WebServer> GetOnlineServers()
        {
            return CloneMonitors().Where(m => m.IsOnline()).Select(m => m.WebServer).ToList();
        }

        public bool IsServerOnline(WebServer webServer)
        {
            return GetOnlineServers().Any(w => w == webServer);
        }

        public void RemoveServer(string serverHost, int serverPort)
        {
            _monitors.RemoveAll(m => m.WebServer.Host == serverHost && m.WebServer.Port == serverPort);
        }

        private void TriggerServersCheck()
        {
            CloneMonitors().ForEach(monitor =>
            {
                ServerCheckHandler?.Invoke(monitor);
            });
        }

        private async Task CheckServers()
        {
            foreach (var monitor in CloneMonitors())
            {
                var monitorResult = await monitor.CheckAsync();

                if (monitorResult.IsOnline)
                {
                    ServerUpHandler?.Invoke(monitor, monitorResult);
                }
                else
                {
                    ServerDownHandler?.Invoke(monitor, monitorResult);
                }
            }
        }

        private List<Monitor> CloneMonitors()
        {
            return _monitors.ToList();
        }
    }
}
