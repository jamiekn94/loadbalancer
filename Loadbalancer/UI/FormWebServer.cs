﻿namespace Loadbalancer.UI
{
    class FormWebServer
    {
        public readonly string Host;
        public readonly int Port;
        public readonly bool IsSecure;

        public FormWebServer(string host, int port, bool isSecure)
        {
            Host = host;
            Port = port;
            IsSecure = isSecure;
        }
    }
}
