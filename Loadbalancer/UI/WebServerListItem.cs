﻿namespace Loadbalancer.UI
{
    class WebServerListItem : FormWebServer
    {
        public WebServerListItem(string host, int port, bool isSecure) : base(host, port, isSecure)
        {
        }

        public override string ToString()
        {
            return $"{Host} {Port}";
        }
    }
}
