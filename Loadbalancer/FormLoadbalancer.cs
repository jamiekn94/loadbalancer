﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Loadbalancer.Distribution;
using Loadbalancer.Health;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;
using Loadbalancer.Persistence;
using Loadbalancer.Tcp;
using Loadbalancer.UI;

namespace Loadbalancer
{
    /// <summary>
    /// Coffee time ☕
    /// </summary>
    public partial class FormLoadbalancer : Form
    {
        private readonly LoadBalancer _loadBalancer;

        private int? _selectedServerIndex;

        public FormLoadbalancer()
        {
            InitializeComponent();

            lvServers.Items.Add(GetListViewItem("localhost", 3001, false));
            lvServers.Items.Add(GetListViewItem("localhost", 3002, false));
            lvServers.Items.Add(GetListViewItem("localhost", 3003, false));
            lvServers.Items.Add(GetListViewItem("localhost", 3004, false));

            _loadBalancer = new LoadBalancer(GetServersFromListItems(lvServers.Items),
                GetDistributionOption(), GetPersistenceOption());
        }

        private void btnAddServerIP_Click(object sender, EventArgs e)
        {
            var server = new WebServerListItem(txtServerIP.Text, int.Parse(txtServerPort.Text),
                cbServerSecure.Checked);

            lvServers.Items.Add(GetListViewItem(server.Host, server.Port, server.IsSecure));

            txtServerIP.Text = string.Empty;
            txtServerPort.Text = string.Empty;
            cbServerSecure.Checked = false;

            _loadBalancer.AddServer(server);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            _loadBalancer.SetDistribution(GetDistributionOption());
            _loadBalancer.SetPersistence(GetPersistenceOption());
            _loadBalancer.OnConnect += OnIncomingConnect;
            _loadBalancer.OnRequest += OnIncomingRequest;
            _loadBalancer.OnClose += OnIncomingClose;
            _loadBalancer.OnServerCheck += OnServerCheck;
            _loadBalancer.OnServerUp += OnServerUp;
            _loadBalancer.OnServerDown += OnServerDown;
            _loadBalancer.Start();

            Log("Loadbalancer started...");

            btnStart.Enabled = false;
            rbPersistenceCookie.Enabled = false;
            rbPersistenceSession.Enabled = false;
            rbPersistenceNone.Enabled = false;
            rbDistributionRoundRobin.Enabled = false;
            rbDistributionRandom.Enabled = false;
        }

        private void OnServerDown(Monitor monitor, MonitorResult monitorResult)
        {
            Log($"Server: {monitor.Url.Host}:{monitor.Url.Port} is down, attempts: {monitorResult.Attempts}");

            var listViewItem = GetListViewItemByHostAndPort(monitor.WebServer.Host, monitor.WebServer.Port);

            if (listViewItem != null)
            {
                listViewItem.ForeColor = Color.White;
                listViewItem.BackColor = monitor.IsOnline() ? Color.Orange : Color.Red;
            }
        }

        private void OnServerUp(Monitor monitor, MonitorResult monitorResult)
        {
            if (monitorResult.Attempts > 1)
            {
                Log(
                    $"Server: {monitor.Url.Host}:{monitor.Url.Port} is back online, downtime: {monitorResult.GetAmountSecondsOffline()} (secs)");
                return;
            }

            Log($"Server: {monitor.Url.Host}:{monitor.Url.Port} is online");

            var listViewItem = GetListViewItemByHostAndPort(monitor.WebServer.Host, monitor.WebServer.Port);

            if (listViewItem != null)
            {
                listViewItem.ForeColor = Color.White;
                listViewItem.BackColor = Color.ForestGreen;
            }
        }

        private void OnServerCheck(Monitor monitor)
        {
            Log($"Checking server: {monitor.Url.Host}:{monitor.Url.Port}");
        }

        private void OnIncomingClose(Connection connection)
        {
            Log("Connection closed");
        }

        private void OnIncomingRequest(Connection connection, Request request)
        {
            Log($"Incoming request: {request.Url}, method: {request.Method}");
        }

        private void OnIncomingConnect(Connection connection)
        {
            Log("New incoming connection");
        }

        private void Log(string message)
        {
            if (lbLog.InvokeRequired)
            {
                Invoke(new Action<string>(Log), message);
                return;
            }

            lbLog.Items.Add(message);
            lbLog.TopIndex = lbLog.Items.Count - 1; // scroll down, why doesnt it do this automatically? makes no sense
        }

        private List<FormWebServer> GetServersFromListItems(ListView.ListViewItemCollection listItems)
        {
            var servers = new List<FormWebServer>();

            foreach (ListViewItem itemRow in listItems)
            {
                string host = null;
                int port = 0;
                bool isSecure = false;

                for (int i = 0; i < itemRow.SubItems.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                        {
                            host = itemRow.SubItems[i].Text;
                            break;
                        }
                        case 1:
                        {
                            port = int.Parse(itemRow.SubItems[i].Text);
                            break;
                        }
                        case 3:
                        {
                            isSecure = bool.Parse(itemRow.SubItems[i].Text);
                            break;
                        }
                    }
                }

                servers.Add(new FormWebServer(host, port, isSecure));
            }

            return servers;
        }

        private ListViewItem GetListViewItem(string host, int port, bool secure)
        {
            var item = new ListViewItem(host);
            item.SubItems.Add(port.ToString());
            item.SubItems.Add(secure.ToString());

            return item;
        }

        private ListViewItem GetListViewItemByHostAndPort(string host, int port)
        {
            if (lvServers.InvokeRequired)
            {
                return
                    (ListViewItem) Invoke(new Func<string, int, ListViewItem>(GetListViewItemByHostAndPort), host, port);
            }

            if (GetServersFromListItems(lvServers.Items).Any(s => s.Host == host && s.Port == port))
            {
                return
                    lvServers.Items[
                        GetServersFromListItems(lvServers.Items).FindIndex(s => s.Host == host && s.Port == port)];
            }

            return null;
        }

        private DistributionOptions GetDistributionOption()
        {
            var distributions = new Dictionary<RadioButton, DistributionOptions>
            {
                {rbDistributionRandom, DistributionOptions.Random},
                {rbDistributionRoundRobin, DistributionOptions.RoundRobin}
            };

            var distribution = distributions.First(d => d.Key.Checked);

            return distribution.Value;
        }

        private PersistenceOptions GetPersistenceOption()
        {
            var persistences = new Dictionary<RadioButton, PersistenceOptions>
            {
                {rbPersistenceCookie, PersistenceOptions.Cookie},
                {rbPersistenceSession, PersistenceOptions.Session},
                {rbPersistenceNone, PersistenceOptions.None}
            };

            var persistence = persistences.First(d => d.Key.Checked);

            return persistence.Value;
        }

        private void lvServers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvServers.SelectedIndices.Count > 0)
            {
                _selectedServerIndex = lvServers.SelectedIndices[0];

                btnDelete.Enabled = true;
            }
            else
            {
                btnDelete.Enabled = false;
                _selectedServerIndex = null;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (_selectedServerIndex.HasValue)
            {
                var server = GetServersFromListItems(lvServers.Items)[_selectedServerIndex.Value];

                _loadBalancer.RemoveServer(server.Host, server.Port);

                lvServers.Items.RemoveAt(_selectedServerIndex.Value);

                _selectedServerIndex = null;
            }
        }
    }
}