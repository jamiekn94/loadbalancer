﻿namespace Loadbalancer
{
    partial class FormLoadbalancer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbLog = new System.Windows.Forms.ListBox();
            this.gbAddServerIP = new System.Windows.Forms.GroupBox();
            this.cbServerSecure = new System.Windows.Forms.CheckBox();
            this.lblSsl = new System.Windows.Forms.Label();
            this.txtServerPort = new System.Windows.Forms.TextBox();
            this.lblServerPort = new System.Windows.Forms.Label();
            this.btnAddServerIP = new System.Windows.Forms.Button();
            this.lblServerHost = new System.Windows.Forms.Label();
            this.txtServerIP = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvServers = new System.Windows.Forms.ListView();
            this.chHost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chSecure = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbPersistence = new System.Windows.Forms.GroupBox();
            this.rbPersistenceNone = new System.Windows.Forms.RadioButton();
            this.rbPersistenceSession = new System.Windows.Forms.RadioButton();
            this.rbPersistenceCookie = new System.Windows.Forms.RadioButton();
            this.lblSessionPersistence = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.rbDistributionRandom = new System.Windows.Forms.RadioButton();
            this.gbDistribution = new System.Windows.Forms.GroupBox();
            this.rbDistributionRoundRobin = new System.Windows.Forms.RadioButton();
            this.btnDelete = new System.Windows.Forms.Button();
            this.gbAddServerIP.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbPersistence.SuspendLayout();
            this.gbDistribution.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.Location = new System.Drawing.Point(22, 12);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(449, 563);
            this.lbLog.TabIndex = 0;
            // 
            // gbAddServerIP
            // 
            this.gbAddServerIP.Controls.Add(this.btnDelete);
            this.gbAddServerIP.Controls.Add(this.cbServerSecure);
            this.gbAddServerIP.Controls.Add(this.lblSsl);
            this.gbAddServerIP.Controls.Add(this.txtServerPort);
            this.gbAddServerIP.Controls.Add(this.lblServerPort);
            this.gbAddServerIP.Controls.Add(this.btnAddServerIP);
            this.gbAddServerIP.Controls.Add(this.lblServerHost);
            this.gbAddServerIP.Controls.Add(this.txtServerIP);
            this.gbAddServerIP.Location = new System.Drawing.Point(958, 22);
            this.gbAddServerIP.Name = "gbAddServerIP";
            this.gbAddServerIP.Size = new System.Drawing.Size(261, 191);
            this.gbAddServerIP.TabIndex = 1;
            this.gbAddServerIP.TabStop = false;
            this.gbAddServerIP.Text = "Server";
            // 
            // cbServerSecure
            // 
            this.cbServerSecure.AutoSize = true;
            this.cbServerSecure.Location = new System.Drawing.Point(85, 95);
            this.cbServerSecure.Name = "cbServerSecure";
            this.cbServerSecure.Size = new System.Drawing.Size(37, 17);
            this.cbServerSecure.TabIndex = 2;
            this.cbServerSecure.Text = "Ja";
            this.cbServerSecure.UseVisualStyleBackColor = true;
            // 
            // lblSsl
            // 
            this.lblSsl.AutoSize = true;
            this.lblSsl.Location = new System.Drawing.Point(13, 96);
            this.lblSsl.Name = "lblSsl";
            this.lblSsl.Size = new System.Drawing.Size(53, 13);
            this.lblSsl.TabIndex = 5;
            this.lblSsl.Text = "Beveiligd:";
            // 
            // txtServerPort
            // 
            this.txtServerPort.Location = new System.Drawing.Point(84, 62);
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Size = new System.Drawing.Size(133, 20);
            this.txtServerPort.TabIndex = 1;
            // 
            // lblServerPort
            // 
            this.lblServerPort.AutoSize = true;
            this.lblServerPort.Location = new System.Drawing.Point(13, 65);
            this.lblServerPort.Name = "lblServerPort";
            this.lblServerPort.Size = new System.Drawing.Size(29, 13);
            this.lblServerPort.TabIndex = 3;
            this.lblServerPort.Text = "Port:";
            // 
            // btnAddServerIP
            // 
            this.btnAddServerIP.Location = new System.Drawing.Point(15, 122);
            this.btnAddServerIP.Name = "btnAddServerIP";
            this.btnAddServerIP.Size = new System.Drawing.Size(202, 23);
            this.btnAddServerIP.TabIndex = 3;
            this.btnAddServerIP.Text = "Add";
            this.btnAddServerIP.UseVisualStyleBackColor = true;
            this.btnAddServerIP.Click += new System.EventHandler(this.btnAddServerIP_Click);
            // 
            // lblServerHost
            // 
            this.lblServerHost.AutoSize = true;
            this.lblServerHost.Location = new System.Drawing.Point(13, 32);
            this.lblServerHost.Name = "lblServerHost";
            this.lblServerHost.Size = new System.Drawing.Size(32, 13);
            this.lblServerHost.TabIndex = 1;
            this.lblServerHost.Text = "Host:";
            // 
            // txtServerIP
            // 
            this.txtServerIP.Location = new System.Drawing.Point(85, 29);
            this.txtServerIP.Name = "txtServerIP";
            this.txtServerIP.Size = new System.Drawing.Size(133, 20);
            this.txtServerIP.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lvServers);
            this.groupBox2.Location = new System.Drawing.Point(512, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(405, 563);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Servers";
            // 
            // lvServers
            // 
            this.lvServers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chHost,
            this.chPort,
            this.chSecure});
            this.lvServers.Location = new System.Drawing.Point(7, 20);
            this.lvServers.MultiSelect = false;
            this.lvServers.Name = "lvServers";
            this.lvServers.Scrollable = false;
            this.lvServers.Size = new System.Drawing.Size(392, 537);
            this.lvServers.TabIndex = 0;
            this.lvServers.UseCompatibleStateImageBehavior = false;
            this.lvServers.View = System.Windows.Forms.View.Details;
            this.lvServers.SelectedIndexChanged += new System.EventHandler(this.lvServers_SelectedIndexChanged);
            // 
            // chHost
            // 
            this.chHost.Text = "Host";
            this.chHost.Width = 252;
            // 
            // chPort
            // 
            this.chPort.Text = "Port";
            this.chPort.Width = 77;
            // 
            // chSecure
            // 
            this.chSecure.Text = "SSL";
            // 
            // gbPersistence
            // 
            this.gbPersistence.Controls.Add(this.rbPersistenceNone);
            this.gbPersistence.Controls.Add(this.rbPersistenceSession);
            this.gbPersistence.Controls.Add(this.rbPersistenceCookie);
            this.gbPersistence.Controls.Add(this.lblSessionPersistence);
            this.gbPersistence.Location = new System.Drawing.Point(958, 241);
            this.gbPersistence.Name = "gbPersistence";
            this.gbPersistence.Size = new System.Drawing.Size(261, 144);
            this.gbPersistence.TabIndex = 3;
            this.gbPersistence.TabStop = false;
            this.gbPersistence.Text = "Persistence";
            // 
            // rbPersistenceNone
            // 
            this.rbPersistenceNone.AutoSize = true;
            this.rbPersistenceNone.Location = new System.Drawing.Point(154, 106);
            this.rbPersistenceNone.Name = "rbPersistenceNone";
            this.rbPersistenceNone.Size = new System.Drawing.Size(51, 17);
            this.rbPersistenceNone.TabIndex = 6;
            this.rbPersistenceNone.TabStop = true;
            this.rbPersistenceNone.Text = "None";
            this.rbPersistenceNone.UseVisualStyleBackColor = true;
            // 
            // rbPersistenceSession
            // 
            this.rbPersistenceSession.AutoSize = true;
            this.rbPersistenceSession.Checked = true;
            this.rbPersistenceSession.Location = new System.Drawing.Point(154, 71);
            this.rbPersistenceSession.Name = "rbPersistenceSession";
            this.rbPersistenceSession.Size = new System.Drawing.Size(62, 17);
            this.rbPersistenceSession.TabIndex = 5;
            this.rbPersistenceSession.TabStop = true;
            this.rbPersistenceSession.Text = "Session";
            this.rbPersistenceSession.UseVisualStyleBackColor = true;
            // 
            // rbPersistenceCookie
            // 
            this.rbPersistenceCookie.AutoSize = true;
            this.rbPersistenceCookie.Location = new System.Drawing.Point(154, 37);
            this.rbPersistenceCookie.Name = "rbPersistenceCookie";
            this.rbPersistenceCookie.Size = new System.Drawing.Size(63, 17);
            this.rbPersistenceCookie.TabIndex = 4;
            this.rbPersistenceCookie.Text = "Cookies";
            this.rbPersistenceCookie.UseVisualStyleBackColor = true;
            // 
            // lblSessionPersistence
            // 
            this.lblSessionPersistence.AutoSize = true;
            this.lblSessionPersistence.Location = new System.Drawing.Point(16, 39);
            this.lblSessionPersistence.Name = "lblSessionPersistence";
            this.lblSessionPersistence.Size = new System.Drawing.Size(104, 13);
            this.lblSessionPersistence.TabIndex = 0;
            this.lblSessionPersistence.Text = "Session persistence:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(958, 552);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(261, 23);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Distribution:";
            // 
            // rbDistributionRandom
            // 
            this.rbDistributionRandom.AutoSize = true;
            this.rbDistributionRandom.Checked = true;
            this.rbDistributionRandom.Location = new System.Drawing.Point(154, 35);
            this.rbDistributionRandom.Name = "rbDistributionRandom";
            this.rbDistributionRandom.Size = new System.Drawing.Size(65, 17);
            this.rbDistributionRandom.TabIndex = 7;
            this.rbDistributionRandom.TabStop = true;
            this.rbDistributionRandom.Text = "Random";
            this.rbDistributionRandom.UseVisualStyleBackColor = true;
            // 
            // gbDistribution
            // 
            this.gbDistribution.Controls.Add(this.rbDistributionRoundRobin);
            this.gbDistribution.Controls.Add(this.label1);
            this.gbDistribution.Controls.Add(this.rbDistributionRandom);
            this.gbDistribution.Location = new System.Drawing.Point(958, 415);
            this.gbDistribution.Name = "gbDistribution";
            this.gbDistribution.Size = new System.Drawing.Size(261, 104);
            this.gbDistribution.TabIndex = 5;
            this.gbDistribution.TabStop = false;
            this.gbDistribution.Text = "Distribution";
            // 
            // rbDistributionRoundRobin
            // 
            this.rbDistributionRoundRobin.AutoSize = true;
            this.rbDistributionRoundRobin.Location = new System.Drawing.Point(154, 67);
            this.rbDistributionRoundRobin.Name = "rbDistributionRoundRobin";
            this.rbDistributionRoundRobin.Size = new System.Drawing.Size(88, 17);
            this.rbDistributionRoundRobin.TabIndex = 8;
            this.rbDistributionRoundRobin.TabStop = true;
            this.rbDistributionRoundRobin.Text = "Round Robin";
            this.rbDistributionRoundRobin.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(15, 151);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(203, 23);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // FormLoadbalancer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 596);
            this.Controls.Add(this.gbDistribution);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.gbPersistence);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gbAddServerIP);
            this.Controls.Add(this.lbLog);
            this.Name = "FormLoadbalancer";
            this.Text = "Loadbalancer";
            this.gbAddServerIP.ResumeLayout(false);
            this.gbAddServerIP.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.gbPersistence.ResumeLayout(false);
            this.gbPersistence.PerformLayout();
            this.gbDistribution.ResumeLayout(false);
            this.gbDistribution.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbLog;
        private System.Windows.Forms.GroupBox gbAddServerIP;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAddServerIP;
        private System.Windows.Forms.Label lblServerHost;
        private System.Windows.Forms.TextBox txtServerIP;
        private System.Windows.Forms.GroupBox gbPersistence;
        private System.Windows.Forms.RadioButton rbPersistenceSession;
        private System.Windows.Forms.RadioButton rbPersistenceCookie;
        private System.Windows.Forms.Label lblSessionPersistence;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtServerPort;
        private System.Windows.Forms.Label lblServerPort;
        private System.Windows.Forms.Label lblSsl;
        private System.Windows.Forms.CheckBox cbServerSecure;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbDistributionRandom;
        private System.Windows.Forms.GroupBox gbDistribution;
        private System.Windows.Forms.RadioButton rbDistributionRoundRobin;
        private System.Windows.Forms.RadioButton rbPersistenceNone;
        private System.Windows.Forms.ListView lvServers;
        private System.Windows.Forms.ColumnHeader chHost;
        private System.Windows.Forms.ColumnHeader chPort;
        private System.Windows.Forms.ColumnHeader chSecure;
        private System.Windows.Forms.Button btnDelete;
    }
}

