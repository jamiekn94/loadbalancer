﻿using System.Collections.Generic;
using System.Linq;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;

namespace Loadbalancer.Persistence
{
    class NoPersistence : IPersistence
    {
        public WebServer GetWebServer(List<WebServer> servers, Request request)
        {
            return null;
        }
    }
}
