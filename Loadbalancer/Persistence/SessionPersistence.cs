﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;
using Loadbalancer.Http.Response;

namespace Loadbalancer.Persistence
{
    class SessionPersistence : IPersistence
    {
        private const string COOKIE_KEY = "connect.sid";
        private readonly ConcurrentDictionary<string, WebServer> _sessionTable;

        public SessionPersistence()
        {
            _sessionTable = new ConcurrentDictionary<string, WebServer>();
        }

        public WebServer GetWebServer(List<WebServer> servers, Request request)
        {
            var sessionCookie = GetRequestCookie(request.Cookies);

            if (string.IsNullOrEmpty(sessionCookie.Value))
            {
                return null;
            }

            if (_sessionTable.ContainsKey(sessionCookie.Value))
            {
                return _sessionTable[sessionCookie.Value];
            }

            return null;
        }

        public void AddSession(RemoteResponse response, WebServer server)
        {
            var sessionCookie =
                response.SetCookies.FirstOrDefault(
                    c => c.Key.Equals(COOKIE_KEY, StringComparison.CurrentCultureIgnoreCase));

            if (!string.IsNullOrEmpty(sessionCookie.Value))
            {
                _sessionTable.TryAdd(sessionCookie.Value, server);
            }
        }

        private KeyValuePair<string, string> GetRequestCookie(Cookies cookies)
        {
            var cookie = cookies.GetValues().FirstOrDefault(c => c.Key == COOKIE_KEY);

            if (string.IsNullOrEmpty(cookie.Value))
            {
                return new KeyValuePair<string, string>();
            }

            return cookie;
        }
    }
}
