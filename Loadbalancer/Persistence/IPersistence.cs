﻿using System.Collections.Generic;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;

namespace Loadbalancer.Persistence
{
    interface IPersistence
    {
        WebServer GetWebServer(List<WebServer> servers, Request request);
    }
}
