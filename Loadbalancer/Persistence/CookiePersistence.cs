﻿using System.Collections.Generic;
using System.Linq;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;

namespace Loadbalancer.Persistence
{
    class CookiePersistence : IPersistence
    {
        public WebServer GetWebServer(List<WebServer> servers, Request request)
        {
            const string webserverCookie = "webserver";

            var webServerCookie = request.Cookies.GetValues().FirstOrDefault(x => x.Key == webserverCookie);

            if (string.IsNullOrEmpty(webServerCookie.Value))
            {
                return null;
            }

            int webServerId = int.Parse(webServerCookie.Value);

            return servers.FirstOrDefault(s => s.Id == webServerId);
        }
    }
}
