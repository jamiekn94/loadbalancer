﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Loadbalancer.Persistence
{
    class PersistenceFactory
    {
        private static PersistenceFactory _instance;

        private readonly IEnumerable<IPersistence> _persistences;

        private PersistenceFactory()
        {
            _persistences = new IPersistence[] {new CookiePersistence(), new SessionPersistence(), new NoPersistence()};
        }

        public static PersistenceFactory GetInstance()
        {
            return _instance ?? (_instance = new PersistenceFactory());
        }

        public IPersistence Get(PersistenceOptions persistence)
        {
            switch (persistence)
            {
                case PersistenceOptions.Cookie:
                {
                    return _persistences.First(p => p is CookiePersistence);
                }
                case PersistenceOptions.Session:
                {
                    return _persistences.First(p => p is SessionPersistence);
                }
                case PersistenceOptions.None:
                    {
                        return _persistences.First(p => p is NoPersistence);
                    }
            }

            throw new NotImplementedException("Unknown persistence.");
        }
    }
}