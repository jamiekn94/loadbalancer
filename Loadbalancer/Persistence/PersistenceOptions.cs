﻿namespace Loadbalancer.Persistence
{
    enum PersistenceOptions
    {
        Cookie, Session, None
    }
}
