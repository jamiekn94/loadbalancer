﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Loadbalancer.Http.Response;

namespace Loadbalancer.Http
{
    /// <summary>
    /// Sends a remote web server request
    /// </summary>
    class HttpClient : IDisposable
    {
        /// <summary>
        /// Request
        /// </summary>
        private readonly Request.Request _request;

        /// <summary>
        /// Remote network stream
        /// </summary>
        private readonly NetworkStream _remoteNetworkStream;

        /// <summary>
        /// Remote tcp client
        /// </summary>
        private readonly TcpClient _remoteTcpClient;

        /// <summary>
        /// Stream which contains the response
        /// </summary>
        private readonly MemoryStream _memoryStream;

        /// <summary>
        /// Size of amount data to read
        /// </summary>
        private const int BUFFER_SIZE = 2048;

        /// <summary>
        /// Is the response fully downloaded?
        /// </summary>
        private bool _isDownloaded;

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="request">Request to send</param>
        /// <param name="webServer"></param>
        public HttpClient(Request.Request request, WebServer webServer)
        {
            _request = request;
            _remoteTcpClient = new TcpClient(webServer.Host, webServer.Port);
            _memoryStream = new MemoryStream();
            _remoteNetworkStream = _remoteTcpClient.GetStream();
            _remoteNetworkStream.ReadTimeout = 5000;

            SendRemoteRequest(_remoteNetworkStream);
        }

        /// <summary>
        /// Read a part of the response
        /// </summary>
        /// <returns></returns>
        public byte[] DownloadRemoteResponse()
        {
            var buffer = new byte[BUFFER_SIZE];

            try
            {
                int bytesRead = _remoteNetworkStream.Read(buffer, 0, buffer.Length);

                _memoryStream.Write(buffer, 0, bytesRead);

                byte[] responseBuffer = new byte[bytesRead];

                Array.Copy(buffer, responseBuffer, bytesRead);

                return responseBuffer;
            }
            catch (IOException)
            {
                _isDownloaded = true;

                return new byte[0];
            }
        }

        /// <summary>
        /// Read the stream into a Response
        /// </summary>
        /// <returns></returns>
        public RemoteResponse GetResponse()
        {
            if (!_isDownloaded)
            {
                ReadToEnd();
            }

            return new RemoteResponse(_memoryStream.ToArray());
        }

        /// <summary>
        /// Read the response untill there is nothing more to read
        /// </summary>
        private void ReadToEnd()
        {
            byte[] buffer;

            do
            {
                buffer = DownloadRemoteResponse();
            } while (buffer.Length > 0);
        }

        /// <summary>
        /// Send a request to the remote webserver
        /// </summary>
        /// <param name="serverNetworkStream">Stream to write to</param>
        private void SendRemoteRequest(NetworkStream serverNetworkStream)
        {
            var request = Encoding.UTF8.GetBytes(GetRequestString());

            serverNetworkStream.Write(request, 0, request.Length);
        }

        /// <summary>
        /// Get a request which includes or excludes several headers
        /// </summary>
        /// <returns>Request</returns>
        private string GetRequestString()
        {
            var headerBuilder = new StringBuilder();
            var includedHeaders = GetIncludeHeaders();
            var excludeHeaders = new List<string>();

            foreach (var header in GetIgnoredHeaders())
            {
                excludeHeaders.Add(header);
            }

            var headers =
                _request.Headers
                    .Available.Where(h => excludeHeaders.All(ih => h.Key != ih)).ToList();

            includedHeaders.ToList().ForEach(h => { headers.Add(new Header(h.Key, h.Value)); });

            return GetHeaderString(headers, headerBuilder);
        }

        /// <summary>
        /// Read the headers into a string
        /// </summary>
        /// <param name="headers"></param>
        /// <param name="headerBuilder"></param>
        /// <returns></returns>
        private string GetHeaderString(List<Header> headers, StringBuilder headerBuilder)
        {
            headers.ForEach(
                header =>
                {
                    headerBuilder.AppendLine(string.IsNullOrEmpty(header.Key)
                        ? header.Value
                        : $"{header.Key}: {header.Value}");
                });

            return headerBuilder + Environment.NewLine;
        }

        /// <summary>
        /// Contains all headers to ignore
        /// </summary>
        /// <returns></returns>
        private string[] GetIgnoredHeaders()
        {
            return new[] { "Connection" };
        }

        /// <summary>
        /// Contains all headers to include into the request
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetIncludeHeaders()
        {
            return new Dictionary<string, string> { { "Connection", "close" } };
        }

        /// <summary>
        /// Dispose all the resources
        /// </summary>
        public void Dispose()
        {
            _remoteNetworkStream?.Dispose();
            _memoryStream?.Dispose();
            _remoteTcpClient?.Close();
        }
    }
}