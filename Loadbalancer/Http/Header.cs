﻿using System;
using System.Collections.Generic;

namespace Loadbalancer.Http
{
    /// <summary>
    /// A single header entry from a request or response
    /// </summary>
    public class Header
    {
        /// <summary>
        /// Initialize a header from a single string
        /// </summary>
        /// <param name="header"></param>
        public Header(string header)
        {
            var keyValueHeader = GetHeader(header);

            Key = keyValueHeader.Key;
            Value = keyValueHeader.Value;
        }

        /// <summary>
        /// Initialize a header from a key and value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public Header(string key, string value)
        {
            Key = key;
            Value = value;
        }

        /// <summary>
        /// Key of header
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Value of header
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Read the header from a single header string
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public KeyValuePair<string, string> GetHeader(string header)
        {
            if (!header.Contains(": "))
            {
                return new KeyValuePair<string, string>(string.Empty, header);
            }

            int index = header.IndexOf(": ", StringComparison.Ordinal);
            string key = header.Substring(0, index);
            string value = header.Substring(index + 1, header.Length - index - 1).Trim();

            return new KeyValuePair<string, string>(key, value);
        }
    }
}