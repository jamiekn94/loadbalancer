﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Loadbalancer.Http
{
    public class Http
    {
        /// <summary>
        /// Split the request by new lines
        /// </summary>
        /// <param name="request">Raw request in string format</param>
        /// <returns></returns>
        protected List<string> GetStringHeaders(string request)
        {
            var lines = request.Split(Environment.NewLine.ToCharArray());
            var headers = new List<string>();
            bool isLineEmpty = false;

            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    if (isLineEmpty)
                    {
                        break;
                    }

                    isLineEmpty = true;

                    continue;
                }

                isLineEmpty = false;

                headers.Add(line);
            }

            return headers;
        }

        protected byte[] GetByteHeaders(List<Header> headers)
        {
            var stringBuilder = new StringBuilder();

            headers.ForEach(header =>
            {
                stringBuilder.AppendLine(string.IsNullOrEmpty(header.Key)
                    ? header.Value
                    : $"{header.Key}: {header.Value}");
            });

            return Encoding.UTF8.GetBytes(stringBuilder.ToString().TrimEnd());
        }
    }
}
