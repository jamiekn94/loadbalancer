﻿namespace Loadbalancer.Http.Response
{
    public interface IResponse
    {
        /// <summary>
        /// Response in byte array
        /// </summary>
        byte[] Data { get; }

        /// <summary>
        /// Status code
        /// </summary>
        int StatusCode { get; }

        /// <summary>
        /// Headers
        /// </summary>
        Headers Headers { get; }
    }
}
