﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Loadbalancer.Http.Response
{
    class OfflineResponse : Http, IResponse
    {
        public byte[] Data { get; }
        public int StatusCode { get; }
        public Headers Headers { get; }

        public OfflineResponse(string message)
        {
            StatusCode = 503;
            Headers = new Headers(GetHeaders(message));
            Data = GetResponse(Headers, message);
        }

        private List<Header> GetHeaders(string body)
        {
            return new List<Header>
            {
                new Header(string.Empty, $"HTTP/1.1 {StatusCode} Service Unavailable"),
                new Header("Date", $"{DateTime.Now.ToUniversalTime():r}"),
                new Header("Connection", "close"),
                new Header("Content-Type", "text/html"),
                new Header("Content-Length", Encoding.ASCII.GetByteCount(body).ToString()),
            };
        }

        private byte[] GetResponse(Headers headers, string body)
        {
            using (var memoryStream = new MemoryStream())
            {
                var bytesHeaders = GetByteHeaders(headers.Available);
                var bytesLineBreak = Encoding.ASCII.GetBytes("\r\n");
                var bytesBody = Encoding.ASCII.GetBytes(body);

                memoryStream.Write(bytesHeaders, 0, bytesHeaders.Length);
                memoryStream.Write(bytesLineBreak, 0, bytesLineBreak.Length);
                memoryStream.Write(bytesLineBreak, 0, bytesLineBreak.Length);
                memoryStream.Write(bytesBody, 0, bytesBody.Length);

                return memoryStream.ToArray();
            }
        }
    }
}
