﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Loadbalancer.Http.Response
{
    /// <summary>
    /// Response from remote server
    /// </summary>
    public class RemoteResponse : Http, IResponse
    {
        private readonly byte[] _rawResponse;

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="response">Response in byte array</param>
        public RemoteResponse(byte[] response)
        {
            _rawResponse = response;   
            _headers = GetStringHeaders(Encoding.UTF8.GetString(response));
        }

        /// <summary>
        /// Response in byte array
        /// </summary>
        public byte[] Data
        {
            get
            {
                var headers = GetByteHeaders(Headers.Available);

                var rawStringBody = Encoding.UTF8.GetString(_rawResponse);

                if(string.IsNullOrEmpty(rawStringBody)) { return new byte[0]; }

                // use regular expression to remove n2c
                string searchString = "\r\n\r\n";

                var indexBody = rawStringBody.IndexOf(searchString, StringComparison.CurrentCultureIgnoreCase);

                var memoryStream = new MemoryStream();

                var body = new byte[rawStringBody.Length - indexBody];

                Array.Copy(_rawResponse, indexBody, body, 0, rawStringBody.Length - indexBody);

                memoryStream.Write(headers, 0, headers.Length);
                memoryStream.Write(body, 0, body.Length);

                var response = memoryStream.ToArray();

                memoryStream.Dispose();

                return response.ToArray();
            }
        }

        /// <summary>
        /// Get the status code
        /// </summary>
        public int StatusCode
        {
            get
            {
                if (!_headers.Any())
                {
                    return -1;
                }

                string firstLine = _headers.First();
                int indexBeforeStatusCode = firstLine.IndexOf(" ", StringComparison.Ordinal);
                int indexAfterStatusCode = firstLine.IndexOf(" ", indexBeforeStatusCode + 1, StringComparison.Ordinal);

                return
                    int.Parse(firstLine.Substring(indexBeforeStatusCode + 1,
                        indexAfterStatusCode - indexBeforeStatusCode - 1));
            }
        }

        public Headers Headers
        {
            get { return new Headers(_headers); }
        }

        public IEnumerable<KeyValuePair<string, string>> SetCookies
        {
            get
            {
                var cookies = Headers.Available.Where(
                   c => c.Key.Equals("set-cookie", StringComparison.CurrentCultureIgnoreCase)).ToList();

                return cookies.Select(c =>
                {
                    int indexIs = c.Value.IndexOf("=", StringComparison.Ordinal);
                    string key = c.Value.Substring(0, indexIs);
                    string value = c.Value.Substring(indexIs + 1, c.Value.IndexOf(";", StringComparison.Ordinal) - indexIs -1);


                    return new KeyValuePair<string, string>(key, value);
                });
            }
        }

        /// <summary>
        /// Response seperated by lines
        /// </summary>
        private readonly List<string> _headers;

        public void AddHeader(Header header)
        {
            _headers.Add($"{header.Key}: {header.Value}");
        }
    }
}