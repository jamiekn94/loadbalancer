﻿using System;
using System.IO;

namespace Loadbalancer.Http
{
    /// <summary>
    /// Reader which writes the buffer to stream so we can read from it whenever there is no more data left
    /// </summary>
    class Reader : IDisposable
    {
        /// <summary>
        /// Holds the data
        /// </summary>
        private readonly MemoryStream _stream;

        /// <summary>
        /// Buffer to read into
        /// </summary>
        public byte[] Buffer;

        /// <summary>
        /// Create a new reader instance
        /// </summary>
        /// <param name="bufferSize">Size of the buffer</param>
        public Reader(int bufferSize)
        {
            Buffer = new byte[bufferSize];
            _stream = new MemoryStream();
        }

        /// <summary>
        /// Write data to the stream
        /// </summary>
        /// <param name="data">Byte array</param>
        /// <param name="bytesRead">Amount of bytes we have read</param>
        public void Write(byte[] data, int bytesRead)
        {
            _stream.Write(data, 0, bytesRead);
        }

        /// <summary>
        /// Read the saved stream
        /// </summary>
        /// <returns>Byte array</returns>
        public byte[] Read()
        {
            return _stream.ToArray();
        }

        /// <summary>
        /// Disposes the buffer and saved stream
        /// </summary>
        public void Dispose()
        {
            Buffer = null;
            _stream?.Dispose();
        }
    }
}