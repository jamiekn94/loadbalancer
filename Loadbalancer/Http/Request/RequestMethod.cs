﻿using System;
using System.Collections.Generic;

namespace Loadbalancer.Http.Request
{
    /// <summary>
    /// Contains all HTTP methods which the proxy supports
    /// </summary>
    class RequestMethod
    {
        /// <summary>
        /// Name of the method
        /// </summary>
        private readonly string _name;

        // All methods
        public static readonly RequestMethod Get = new RequestMethod("GET");
        public static readonly RequestMethod Post = new RequestMethod("POST");
        public static readonly RequestMethod Put = new RequestMethod("PUT");
        public static readonly RequestMethod Delete = new RequestMethod("DELETE");
        public static readonly RequestMethod Head = new RequestMethod("HEAD");
        public static readonly RequestMethod Connect = new RequestMethod("Connect");
        public static readonly RequestMethod Unknown = new RequestMethod("Unknown");

        /// <summary>
        /// List of all HTTP methods
        /// </summary>
        public static readonly List<RequestMethod> Methods = new List<RequestMethod>
        {
            Get,
            Post,
            Post,
            Delete,
            Head,
            Connect,
            Unknown
        };

        /// <summary>
        /// Retrieve a RequestMethod by name
        /// </summary>
        /// <param name="pMethod"></param>
        /// <returns></returns>
        public static RequestMethod Retrieve(string pMethod)
        {
            var method = Methods.Find(m => m.ToString().Equals(pMethod, StringComparison.CurrentCultureIgnoreCase));

            if (method == null)
            {
                return Unknown;
            }

            return method;
        }

        /// <summary>
        /// Initialize a RequestMethod
        /// </summary>
        /// <param name="name">Method name</param>
        private RequestMethod(string name)
        {
            _name = name;
        }

        /// <summary>
        /// Override the ToString method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _name;
        }
    }
}