﻿using System;

namespace Loadbalancer.Http.Request
{
    /// <summary>
    /// Parser which is used to read parts of a string
    /// </summary>
    class Parser
    {
        /// <summary>
        /// Reader
        /// </summary>
        private string _reader;

        /// <summary>
        /// Initialize the parser
        /// </summary>
        /// <param name="message">Value</param>
        public Parser(string message)
        {
            _reader = string.Copy(message);
        }

        /// <summary>
        /// Read a part of the string
        /// </summary>
        /// <param name="readTill">Value to stop reading</param>
        /// <returns>Substring of reader</returns>
        public string Read(string readTill)
        {
            int index = _reader.IndexOf(readTill, 0, StringComparison.Ordinal);

            string subString = _reader.Substring(0, index);

            _reader = _reader.Substring(index + 1, _reader.Length - index - 1);

            return subString;
        }
    }
}