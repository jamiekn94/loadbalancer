﻿using System;
using System.Collections.Generic;

namespace Loadbalancer.Http.Request
{
    /// <summary>
    /// Remote Request
    /// </summary>
    class Request : Http
    {
        /// <summary>
        /// HTTP Method
        /// </summary>
        public RequestMethod Method { get; private set; }

        /// <summary>
        /// Url of request
        /// </summary>
        public Uri Url { get; private set; }

        public Headers Headers
        {
            get
            {
                return new Headers(StringHeaders);
            }
        }

        public Cookies Cookies
        {
            get
            {
                return new Cookies(Headers);
            }
        }

        private List<string> StringHeaders { get; set; }

        /// <summary>
        /// Initialize a request
        /// </summary>
        /// <param name="request">Raw request</param>
        public Request(string request)
        {
            StringHeaders = GetStringHeaders(request);

            var parser = new Parser(request);

            // todo: remove parser

            Method = GetRequestMethod(parser);
            Url = GetUrl(parser);
        }

        public void AddCookie(string key, string value)
        {
            StringHeaders.Add($"Set-Cookie: {key}={value}");
        }

        /// <summary>
        /// Read the url from the request
        /// </summary>
        /// <param name="parser"></param>
        /// <returns></returns>
        private Uri GetUrl(Parser parser)
        {
            string url = parser.Read(" ");
            return new Uri(url);
        }

        /// <summary>
        /// Read the HTTP method from the request
        /// </summary>
        /// <param name="parser"></param>
        /// <returns></returns>
        private RequestMethod GetRequestMethod(Parser parser)
        {
            return RequestMethod.Retrieve(parser.Read(" "));
        }
    }
}