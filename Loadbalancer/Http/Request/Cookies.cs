﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Loadbalancer.Http.Request
{
    class Cookies
    {
        private readonly Headers _headers;

        public Cookies(Headers headers)
        {
            _headers = headers;
        }

        public Cookies(Header header)
        {
            _headers = new Headers(new List<Header> {header});
        }

        public IEnumerable<KeyValuePair<string, string>> GetValues()
        {
            var cookie = _headers.Available.FirstOrDefault(h => h.Key == "Cookie");

            if (cookie == null)
            {
                return new KeyValuePair<string, string>[0];
            }

            var lines = cookie.Value.Split(';');

            var keyValues = lines.Select(c =>
            {
                int indexIs = c.IndexOf("=", StringComparison.Ordinal);
                string key = c.Substring(0, indexIs).Trim();
                string value = c.Substring(indexIs + 1);

                return new KeyValuePair<string, string>(key, value);
            });

            return keyValues;
        }
    }
}
