﻿using System.Collections.Generic;
using System.Linq;

namespace Loadbalancer.Http
{
    /// <summary>
    /// Class which contains all headers
    /// </summary>
    public class Headers
    {
        /// <summary>
        /// List of headers
        /// </summary>
        public List<Header> Available { get; private set; }

        /// <summary>
        /// Initialize and read all the headers
        /// </summary>
        /// <param name="requestLines">Raw request which is seperated by lines</param>
        public Headers(IEnumerable<string> requestLines)
        {
            Available = GetHeaders(requestLines);
        }

        public Headers(List<Header> headers)
        {
            Available = headers;
        }

        /// <summary>
        /// Read all the headers from a request
        /// </summary>
        /// <param name="requestLines">Raw request which is seperated by lines</param>
        /// <returns></returns>
        private List<Header> GetHeaders(IEnumerable<string> requestLines)
        {
            return requestLines.Select(rl => new Header(rl)).ToList();
        }
    }
}