﻿using System.Collections.Generic;
using System.Linq;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;

namespace Loadbalancer.Distribution
{
    class RoundRobinDistribution : IDistribution
    {
        private object _lock;
        private int _lastWebServerIndex;
        private int _lastWebserverId;

        public RoundRobinDistribution()
        {
            _lock = new object();
        }

        public WebServer GetWebServer(List<WebServer> servers, Request request)
        {
            lock (_lock)
            {
                int maxId = servers.Max(s => s.Id);

                if (_lastWebserverId >= maxId)
                {
                    var firstServer = servers.First();
                    _lastWebserverId = firstServer.Id;

                    return firstServer;
                }

                var index = servers.FindIndex(s => s.Id > _lastWebserverId);
                var server = servers[index];

                _lastWebserverId = server.Id;

                return server;

                /*  if (_lastWebServerIndex > (servers.Count - 1))
                  {
                      _lastWebServerIndex = 0;
                  }
  
                  var server = servers[_lastWebServerIndex];
  
                  _lastWebServerIndex++;
  
                  return server;*/
            }
        }
    }
}
