﻿namespace Loadbalancer.Distribution
{
    enum DistributionOptions
    {
        Random, RoundRobin
    }
}
