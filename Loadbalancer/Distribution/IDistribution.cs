﻿using System.Collections.Generic;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;

namespace Loadbalancer.Distribution
{
    interface IDistribution
    {
        WebServer GetWebServer(List<WebServer> servers, Request request);
    }
}
