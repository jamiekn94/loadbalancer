﻿using System;
using System.Collections.Generic;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;

namespace Loadbalancer.Distribution
{
    class RandomDistribution : IDistribution
    {
        private readonly Random _random;

        public RandomDistribution()
        {
            _random = new Random();
        }

        public WebServer GetWebServer(List<WebServer> servers, Request request)
        {
            int randomWebServerIndex = _random.Next(0, servers.Count);

            return servers[randomWebServerIndex];
        }
    }
}
