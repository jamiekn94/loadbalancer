﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loadbalancer.Persistence;

namespace Loadbalancer.Distribution
{
    class DistributionFactory
    {
        private static DistributionFactory _instance;

        private readonly IEnumerable<IDistribution> _distributions;

        private DistributionFactory()
        {
            _distributions = new IDistribution[] {new RandomDistribution(), new RoundRobinDistribution()};
        }

        public static DistributionFactory GetInstance()
        {
            return _instance ?? (_instance = new DistributionFactory());
        }

        public IDistribution Get(DistributionOptions distribution)
        {
            switch (distribution)
            {
                case DistributionOptions.Random:
                {
                    return _distributions.First(d => d is RandomDistribution);
                }
                case DistributionOptions.RoundRobin:
                {
                    return _distributions.First(d => d is RoundRobinDistribution);
                }
            }

            throw new NotImplementedException("Unknown distribution.");
        }
    }
}