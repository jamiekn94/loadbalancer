﻿using System;
using Loadbalancer.UI;

namespace Loadbalancer
{
    class WebServer : FormWebServer
    {
        public readonly int Id;

        public WebServer(string host, int port, bool isSecure, int id) : base(host, port, isSecure)
        {
            Id = id;
        }
    }
}
