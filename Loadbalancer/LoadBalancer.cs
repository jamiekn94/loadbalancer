﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loadbalancer.Distribution;
using Loadbalancer.Health;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;
using Loadbalancer.Http.Response;
using Loadbalancer.Persistence;
using Loadbalancer.Tcp;
using Loadbalancer.UI;

namespace Loadbalancer
{
    class LoadBalancer
    {
        public delegate void DelOnConnectionHandler(Connection connection);

        public delegate void DelOnConnectionCloseHandler(Connection connection);

        public delegate void DelOnConnectionRequestHandler(Connection connection, Request request);

        public event DelOnConnectionCloseHandler OnConnect;
        public event DelOnConnectionCloseHandler OnClose;
        public event DelOnConnectionRequestHandler OnRequest;

        public event HealthMonitor.ServerCheck OnServerCheck;
        public event HealthMonitor.ServerUp OnServerUp;
        public event HealthMonitor.ServerDown OnServerDown;

        private readonly HealthMonitor _healthMonitor;
        private readonly TcpSocketServer _tcpSocketServer;
        private IDistribution _distribution;
        private IPersistence _persistence;
        private readonly List<WebServer> _webServers;
        private DistributionOptions _distributionOption;
        private PersistenceOptions _persistenceOption;

        private int _lastServerId;
        private bool _isLoadBalancerStarted;

        public LoadBalancer(List<FormWebServer> formWebServers, DistributionOptions distribution,
            PersistenceOptions persistence)
        {
            _webServers = new List<WebServer>();

            _healthMonitor = new HealthMonitor();
            _tcpSocketServer = new TcpSocketServer();
            _distribution = DistributionFactory.GetInstance().Get(distribution);
            _persistence = PersistenceFactory.GetInstance().Get(persistence);
            _distributionOption = distribution;
            _persistenceOption = persistence;

            _healthMonitor.ServerCheckHandler += HandleServerCheck;
            _healthMonitor.ServerUpHandler += HandleServerUp;
            _healthMonitor.ServerDownHandler += HandleServerDown;

            formWebServers.ForEach(AddServer);
        }

        public void Start()
        {
            _isLoadBalancerStarted = true;
            _healthMonitor.Start();
            _tcpSocketServer.Start();
            _tcpSocketServer.OnIncomingConnection += HandleIncomingConnection;
        }

        public void AddServer(FormWebServer formWebServer)
        {
            int generatedId = ++_lastServerId;

            var server = new WebServer(formWebServer.Host, formWebServer.Port, formWebServer.IsSecure, generatedId);

            _webServers.Add(server);
            _healthMonitor.AddServer(server);
        }

        public void SetDistribution(DistributionOptions distribution)
        {
            if(_isLoadBalancerStarted) { throw new InvalidOperationException("You cannot set the distribution once the loadbalancer has started."); }

            _distributionOption = distribution;
            _distribution = DistributionFactory.GetInstance().Get(distribution);
        }

        public void SetPersistence(PersistenceOptions persistence)
        {
            if (_isLoadBalancerStarted) { throw new InvalidOperationException("You cannot set the persistence once the loadbalancer has started."); }

            _persistenceOption = persistence;
            _persistence = PersistenceFactory.GetInstance().Get(persistence);
        }

        public void RemoveServer(string serverHost, int serverPort)
        {
            _webServers.RemoveAll(w => w.Host == serverHost && w.Port == serverPort);
            _healthMonitor.RemoveServer(serverHost, serverPort);
        }

        private void HandleIncomingConnection(Connection connection)
        {
            OnConnect?.Invoke(connection);

            connection.OnRequest += HandleIncomingRequest;
            connection.OnClose += HandleConnectionClose;
        }

        private void HandleConnectionClose(Connection connection)
        {
            OnClose?.Invoke(connection);
        }

        private void HandleIncomingRequest(Connection connection, Request request)
        {
            OnRequest?.Invoke(connection, request);

            var onlineServers = _healthMonitor.GetOnlineServers();

            var persistenceWebServer = _persistence.GetWebServer(_webServers, request);

            if (persistenceWebServer != null && !_healthMonitor.IsServerOnline(persistenceWebServer))
            {
                SendPersistenceServerDownResponse(connection);
                return;
            }

            if (!onlineServers.Any())
            {
                SendNoServerAvailableResponse(connection);
                return;
            }

            WebServer webServer = persistenceWebServer ?? _distribution.GetWebServer(onlineServers, request);

            Header extraResponseHeader = null;

            if (_persistenceOption == PersistenceOptions.Cookie && persistenceWebServer == null)
            {
                extraResponseHeader = new Header("Set-Cookie", $"webserver={webServer.Id}");
            }

            var response = Send(connection, request, webServer, extraResponseHeader);

            if (_persistenceOption == PersistenceOptions.Session && persistenceWebServer == null)
            {
                var sessionPersistence = (SessionPersistence) _persistence;
                sessionPersistence.AddSession(response, webServer);
            }
        }

        private RemoteResponse Send(Connection connection, Request request, WebServer webServer,
            Header extraResponseHeader = null)
        {
            using (var httpClient = new HttpClient(request, webServer))
            {
                var response = httpClient.GetResponse();

                if (extraResponseHeader != null)
                {
                    response.AddHeader(extraResponseHeader);
                }

                connection.Send(response.Data);

                connection.Disconnect();

                return response;
                ;
            }
        }

        private void SendPersistenceServerDownResponse(Connection connection)
        {
            connection.Send(
                new OfflineResponse("Your persistence server is currently down. Please wait a few seconds/minutes.")
                    .Data);
            connection.Disconnect();
        }

        private void SendNoServerAvailableResponse(Connection connection)
        {
            connection.Send(
                new OfflineResponse(
                    "There aren't any servers online to process your request. Please wait a few seconds/minutes.").Data);
            connection.Disconnect();
        }

        private void HandleServerCheck(Monitor monitor)
        {
            OnServerCheck?.Invoke(monitor);
        }

        private void HandleServerUp(Monitor monitor, MonitorResult monitorResult)
        {
            OnServerUp?.Invoke(monitor, monitorResult);
        }

        private void HandleServerDown(Monitor monitor, MonitorResult monitorResult)
        {
            OnServerDown?.Invoke(monitor, monitorResult);
        }
    }
}