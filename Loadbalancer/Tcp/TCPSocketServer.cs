﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Loadbalancer.Tcp
{
    class TcpSocketServer
    {
        /// <summary>
        /// Delegate which is whenever we receive an incoming connection
        /// </summary>
        /// <param name="connection">Client connection</param>
        public delegate void OnIncomingConnectionHandler(Connection connection);

        /// <summary>
        /// Event which you can un(subscribe) to receive incoming connection updates
        /// </summary>
        public event OnIncomingConnectionHandler OnIncomingConnection;

        /// <summary>
        /// Tcp listener
        /// </summary>
        private readonly TcpListener _tcpListener;

        /// <summary>
        /// Thread safe list which holds all the connections
        /// </summary>
        private readonly ConcurrentBag<Connection> _connections;

        /// <summary>
        /// Thread which runs the TCP listener
        /// </summary>
        private Thread _thread;

        /// <summary>
        /// Buffer size
        /// </summary>
        private readonly int _bufferSize;

        /// <summary>
        /// Create a server instance
        /// </summary>
        public TcpSocketServer()
        {
            _tcpListener = new TcpListener(IPAddress.Any, 81);
            _connections = new ConcurrentBag<Connection>();
            _bufferSize = 2048;
        }

        /// <summary>
        /// Start the server in a thread
        /// </summary>
        public void Start()
        {
            _tcpListener.Start();

            _thread = new Thread(StartListening) { IsBackground = true };
            _thread.Start();
        }

        /// <summary>
        /// Get the amount of connections currently connected
        /// </summary>
        /// <returns></returns>
        public int GetAmountConnections()
        {
            return _connections.Count;
        }

        /// <summary>
        /// Register a callback for accepting incoming connections
        /// </summary>
        private void StartListening()
        {
            _tcpListener.BeginAcceptTcpClient(AcceptConnection, null);
        }

        /// <summary>
        /// Method which is run whenenever a connection tries to connect
        /// </summary>
        /// <param name="result">Asynchronous data result</param>
        private void AcceptConnection(IAsyncResult result)
        {
            var client = _tcpListener.EndAcceptTcpClient(result);

            var connection = AddConnection(client);

            OnIncomingConnection?.Invoke(connection);

            _tcpListener.BeginAcceptTcpClient(AcceptConnection, null);

            connection.Listen();
        }

        /// <summary>
        /// Register the connection
        /// </summary>
        /// <param name="tcpClient">Tcp client from the user</param>
        /// <returns>Created connection</returns>
        private Connection AddConnection(TcpClient tcpClient)
        {
            var connection = new Connection(tcpClient, _bufferSize);

            connection.OnClose += OnConnectionClosed;

            _connections.Add(connection);

            return connection;
        }

        #region Events

        /// <summary>
        /// OnConnectionClosed method which is called whenever we a connection of a user is lost
        /// </summary>
        /// <param name="connection">Connection of the user</param>
        private void OnConnectionClosed(Connection connection)
        {
            _connections.TryTake(out connection);
        }

        #endregion
    }
}
