﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using Loadbalancer.Http;
using Loadbalancer.Http.Request;

namespace Loadbalancer.Tcp
{
    /// <summary>
    /// User connection which handles a specific user connection
    /// </summary>
    class Connection
    {
        /// <summary>
        /// Size of buffer
        /// </summary>
        public readonly int BufferSize;

        /// <summary>
        /// TCP Client
        /// </summary>
        private TcpClient _tcpClient;

        /// <summary>
        /// OnConnection delegate which is used whenever the connection succesfully connected
        /// </summary>
        /// <param name="connection">Clients connection which succesfully connected</param>
        public delegate void DelOnConnectionCloseHandler(Connection connection);

        public delegate void DelOnConnectionRequestHandler(Connection connection, Request request);

        /// <summary>
        /// Event which you can subscribe to whenever the clients connection is closed
        /// </summary>
        public event DelOnConnectionCloseHandler OnClose;

        public event DelOnConnectionRequestHandler OnRequest;

        /// <summary>
        ///  Initialize
        /// </summary>
        /// <param name="tcpClient">Client</param>
        /// <param name="bufferSize">Buffer size</param>
        public Connection(TcpClient tcpClient, int bufferSize)
        {
            _tcpClient = tcpClient;
            BufferSize = bufferSize;
        }

        /// <summary>
        /// Listen to new incoming messages
        /// </summary>
        public void Listen()
        {
            var reader = new Reader(BufferSize);

            StartReading(reader);
        }

        /// <summary>
        /// Send a message to the user
        /// </summary>
        /// <param name="message">Information about the message</param>
        public void Send(string message)
        {
            var outGoingMessage = Encoding.UTF8.GetBytes(message);
            _tcpClient.GetStream().Write(outGoingMessage, 0, outGoingMessage.Length);
        }

        /// <summary>
        /// Send byte array to client
        /// </summary>
        /// <param name="data">Data to send</param>
        public void Send(byte[] data)
        {
            _tcpClient.GetStream().Write(data, 0, data.Length);
        }

        /// <summary>
        /// Disconnect the user
        /// </summary>
        public void Disconnect()
        {
            if (_tcpClient == null) { return; }

            _tcpClient.Close();

            // Check if the user is still connected
            if (_tcpClient.Connected)
            {
                _tcpClient.GetStream().Close();
            }

            _tcpClient = null;

            OnClose?.Invoke(this);
        }

        /// <summary>
        /// Register a callback for incoming messages
        /// </summary>
        /// <param name="reader">Message reader</param>
        private void StartReading(Reader reader)
        {
            _tcpClient.GetStream().BeginRead(reader.Buffer, 0, reader.Buffer.Length, OnIncomingData, reader);
        }

        /// <summary>
        /// Method which is called whenever we receive data
        /// </summary>
        /// <param name="iAr">Information about the asynchronous call</param>
        private void OnIncomingData(IAsyncResult iAr)
        {
            var reader = (Reader)iAr.AsyncState;

            // Try to read from the stream
            try
            {
                int bytesRead = _tcpClient.GetStream().EndRead(iAr);

                // Check if the connection is still open
                if (bytesRead == 0)
                {
                    reader.Dispose();
                    Disconnect();
                    return;
                }

                reader.Write(reader.Buffer, bytesRead);

                // Check if there is any data left to read
                if (_tcpClient.GetStream().DataAvailable)
                {
                    StartReading(reader);
                    return;
                }

                OnCompleteReadingData(reader.Read());

                reader.Dispose();
            }
            // Catch socket and io exception and close the connection
            catch (Exception ex)
            {
                if (ex is SocketException || ex is IOException)
                {
                    Disconnect();
                    return;
                }
                throw;
            }
        }

        /// <summary>
        /// Method which is called whenever we read the full message
        /// </summary>
        /// <param name="data">Information about the message</param>
        private void OnCompleteReadingData(byte[] data)
        {
            string requestHeader = Encoding.ASCII.GetString(data);

            var request = new Request(requestHeader);
            
            OnRequest?.Invoke(this, request);

            Disconnect();
        }
    }
}