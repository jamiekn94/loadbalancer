# README #

## Beschrijving applicatie ##

De loadbalancer maakt gebruik van een active monitoring systeem. Dit monitoring systeem controleert om de 5 seconden of een server online is. Als de server 3x achter elkaar offline is geweest zal hij niet meer gebruikt worden om requests naar toe te sturen. Als de gebruiker een session of cookie heeft en de server die hierbij hoort offline is zal een 503 Service Unavailable response verstuurd worden naar de gebruiker. Deze code houd het volgende in "The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state.". Dit is precies de situatie waarin de gebruiker zich in bevind. De server zal waarschijnlijk tijdelijk offline en (hopelijk) binnen enkele minuten weer online komen. Als de gebruiker geen session persistence heeft dan zal het distribution algorithme een server kiezen. Bijvoorbeeld round robin of random. 

### Extra informatie ###

Om de server te testen heb ik de NodeJS web servers van Theo gecloned en een aantal settings aangepast. De Loadbalancer bevat standaard deze servers. 
Je kan deze servers starten door de ".bat" files. Al deze web servers zijn te vinden in de map "NodeJS Servers". Voordat je de NodeJS web servers kan draaien moet je eerst een NPM install doen in de NodeJS Servers map. De pagina die alle web servers serveren geven ook aan welke server jouw request heeft geserveerd.

De web servers bevatten de volgende configuratie:

** Server 1 **

- Altijd online

- Session verloopt na 5 seconden

** Server 2 **

- Altijd online

- Session verloopt na 5 seconden

** Server 3 **

- Offline om de 60 seconden, voor 60 seconden

- Session verloopt na 20 seconden

** Server 4 **

- Offline om de 30 seconden, voor 30 seconden

- Session verloopt na 120 seconden

### Round robin geeft verkeerde server terug? ###
Na een paar keer over mijn hoofd gekrabt te hebben waarom ik server 1 terug krijg wanneer ik eigenlijk server 4 verwacht komt omdat FireFox nogmaals een request maakt wanneer het een slechte response ontvangt. Omdat de load balancer een server pas offline beschouwt na 3 pogingen wordt de server daarvoor nog wel gebruikt om requests naar toe te verzenden. Omdat server 4 wel de connectie accepteert maar abrupt de connectie verbreekt probeert FireFox nogmaals te verbinden. Hierdoor krijg je een nieuwe server. In dit geval nummer 1. 

## Class diagram ##

![class-diagram-loadbalancer.png](https://bitbucket.org/repo/LoRd969/images/3589795519-class-diagram-loadbalancer.png)

Uiteindelijk heb ik mij voor een groot deel gehouden aan het class diagram. Er zijn wel een aantal classes toegevoegd omdat ik tijdens het programmeren merkte dat ik deze extra classes nodig had.

## Sequence diagram ##

Het sequence diagram geeft in het kort weer hoe de flow loopt wanneer de load balancer wordt opgestart.

![load-balancer-flow.png](https://bitbucket.org/repo/LoRd969/images/4180891698-load-balancer-flow.png)

## Verdeling van load over HTTP-Servers ##

**Beschrijving van concept**

Een load balancer is een applicatie die voor web servers zit. Load balancers houden bij welke web servers online zijn zodat requests alleen naar doorgestuurd worden naar servers die online zijn. Het biedt ook de mogelijkheid om requests te verdelen over meerdere servers door algorithmes zoals round robin. Door requests te verdelen over meerdere servers kunnen meerdere gebruikers worden ondersteund. Vaak bieden load balancers ook functionaliteiten voor session persistence. Session persistence is nodig wanneer een web applicatie bijvoorbeeld ingelogde gebruikers bijhoud. De gebruiker zal alleen ingelogd zijn op de server waar hij heeft ingelogd. Als de load balancer een andere server aan de gebruiker aanwijst waarin hij niet is ingelogd zal hij opnieuw moeten ingelogd. Dit is wat session persistence voorkomt. De loadbalancer zal hierbij bijhouden welke sessie bij welke server hoort. Session persistence kan op meerdere manieren geimplemeneerd worden. Bijvoorbeeld door middel van responses uit te lezen controleren of de web server een sessie heeft gezet. De loadbalancer zal hierbij een eigen session table bijhouden waarin de sessie en de bijbehorende server worden opgeslagen. Een andere mogelijkheid is dat de load balancer een cookie toevoegt waarin een value staat die verbonden is aan aan specifieke webserver. 

**Code voorbeeld**

De code hieronder is verantwoordelijk voor het zoeken voor een juiste server voor de request. Zo kijkt het eerste of de gebruiker een session heeft die gekoppeld is aan een webserver. Als de gebruiker geen session heeft dan komt de distribution algorithme aan de pas die server kiest.


```
#!C#

 private void HandleIncomingRequest(Connection connection, Request request)
        {
            OnRequest?.Invoke(connection, request);

            var onlineServers = _healthMonitor.GetOnlineServers();

            var persistenceWebServer = _persistence.GetWebServer(_webServers, request);

            if (persistenceWebServer != null && !_healthMonitor.IsServerOnline(persistenceWebServer))
            {
                SendPersistenceServerDownResponse(connection);
                return;
            }

            if (!onlineServers.Any())
            {
                SendNoServerAvailableResponse(connection);
                return;
            }

            WebServer webServer = persistenceWebServer ?? _distribution.GetWebServer(onlineServers, request);

            Header extraResponseHeader = null;

            if (_persistenceOption == PersistenceOptions.Cookie && persistenceWebServer == null)
            {
                extraResponseHeader = new Header("Set-Cookie", $"webserver={webServer.Id}");
            }

            var response = Send(connection, request, webServer, extraResponseHeader);

            if (_persistenceOption == PersistenceOptions.Session && persistenceWebServer == null)
            {
                var sessionPersistence = (SessionPersistence) _persistence;
                sessionPersistence.AddSession(response, webServer);
            }
        }
```


**Alternatieven**

Een alternatief voor load balancing is een failover server. Deze server treed in werking wanneer de main server offline gaat. Op deze manier voorkom je dat gebruikers geen toegang hebben tot de website. Een algemene failover server biedt alleen niet de mogelijkheid om requests te verdelen over meerdere servers.

**Authentieke en gezaghebbende bronnen**

https://www.nginx.com/resources/glossary/load-balancing/

https://www.fxw.nl/failover.html

## Session Persistence implementatie, keuzes en algoritmiek ##
Twee session persistence implementaties zijn geimplementeerd, namelijk: cookie en session table based.

**Beschrijving van concept**

Wanneer gekozen wordt voor cookie based persistence wordt in de cookie een verwijziging opgeslagen naar een web server. Als de gebruiker voor de tweede keer een request maakt zal de load balancer hem doorverwijzen naar de server die hoort bij de cookie waarde. Bij session table based wordt de response uitgelezen en wordt de sessie die meegestuurd is uitgelezen. Deze sessie wordt vervolgens opgeslagen in de table inclusief de server die deze sessie heeft meegestuurd. Bij vervolg requests wordt de gebruiker doorverwezen aan de server die gekoppeld is aan de sessie.

**Code voorbeeld**

De session persistence class houd een lijst bij met sessies inclusief servers die horen bij een bepaalde sessie. Zo bevat het ook functionaliteiten om naar sessie's te zoeken in een response en om een sessie toe te voegen in de session table.


```
#!C#

class SessionPersistence : IPersistence
    {
        private const string COOKIE_KEY = "connect.sid";
        private readonly ConcurrentDictionary<string, WebServer> _sessionTable;

        public SessionPersistence()
        {
            _sessionTable = new ConcurrentDictionary<string, WebServer>();
        }

        public WebServer GetWebServer(List<WebServer> servers, Request request)
        {
            var sessionCookie = GetRequestCookie(request.Cookies);

            if (string.IsNullOrEmpty(sessionCookie.Value))
            {
                return null;
            }

            if (_sessionTable.ContainsKey(sessionCookie.Value))
            {
                return _sessionTable[sessionCookie.Value];
            }

            return null;
        }

        public void AddSession(RemoteResponse response, WebServer server)
        {
            var sessionCookie =
                response.SetCookies.FirstOrDefault(
                    c => c.Key.Equals(COOKIE_KEY, StringComparison.CurrentCultureIgnoreCase));

            if (!string.IsNullOrEmpty(sessionCookie.Value))
            {
                _sessionTable.TryAdd(sessionCookie.Value, server);
            }
        }

        private KeyValuePair<string, string> GetRequestCookie(Cookies cookies)
        {
            var cookie = cookies.GetValues().FirstOrDefault(c => c.Key == COOKIE_KEY);

            if (string.IsNullOrEmpty(cookie.Value))
            {
                return new KeyValuePair<string, string>();
            }

            return cookie;
        }
    }
```


**Alternatieven**

NGINX een bekende load balancer ondersteund drie persistence methodes. Namelijk de bekende cookie en sessie table persistence, maar ook een learn persistence. Bij deze methode worden requests en responses om sessie identifiers te vinden. De loadbalancer leert hierdoor welke server hoort bij een bepaalde identifier. Zodra een bekende identifier is gevonden zal de loadbalancer de request doorsturen naar de server die hoort bij deze identifier.

**Authentieke en gezaghebbende bronnen**

https://www.nginx.com/products/session-persistence/

## Health Monitoring implementatie, keuzes en algoritmiek ##

**Beschrijving van concept**

Health monitoring wordt gebruikt om te controleren of een server online of offline is. Dit is een cruciaal element voor load balancers omdat je geen requests wilt doorsturen naar servers die offline zijn. Er zijn meerdere methodes om te controleren of een server nog online is. Een van deze methodes is active monitoring. Dit is een losstaand proces die om de x aantal minuten/seconden een request maakt om te controleren of de server nog een acceptabele response stuurt. Een andere methode is passive monitoring. Bij deze methode ga je meeluisteren naar responses van user requests. De loadbalancer zal dus zelf geen requests sturen naar een web server. 

**Code voorbeeld**

In het voorbeeld hieronder staat de code die controleert of de server online of offline is. Een server wordt beschouwd als offline wanneer hij geen 200 OK status bevat. Deze code bevat ook de logica die bijhoudt hoe vaak geprobeerd is om opnieuw te verbinden met de server.


```
#!C#

 public async Task<MonitorResult> CheckAsync()
        {
            bool isOkStatusCode = true;
            int monitorResultFailedConnectAttempts;
            DateTime? monitorResultDateDown = null;

            try
            {
                var response = await DoRequestAsync();

                isOkStatusCode = ((HttpWebResponse) response).StatusCode == HttpStatusCode.OK;

                response.Dispose();
            }
            catch (WebException)
            {
                isOkStatusCode = false;
            }
            finally
            {
                if (isOkStatusCode)
                {
                    monitorResultDateDown = _dateDown;
                    monitorResultFailedConnectAttempts = _failedConnectAttempts + 1;

                    _dateDown = null;
                    _failedConnectAttempts = 0;
                }
                else
                {
                    if (_failedConnectAttempts == 0)
                    {
                        _dateDown = DateTime.Now;

                        monitorResultDateDown = _dateDown;
                    }

                    _failedConnectAttempts++;

                    monitorResultFailedConnectAttempts = _failedConnectAttempts;
                }
            }

            return new MonitorResult(monitorResultFailedConnectAttempts, monitorResultDateDown, isOkStatusCode);
        }
```


**Alternatieven**

Een alternatief naast health monitoring zou een third party monitoring tool zijn zoals Pingdom en UptimeRobot. Dit zijn services die om de x aantal seconden/minuten controleren of jouw applicatie nog wel online is en of hij een valide status code bevat. Zodra een applicatie offline is zullen deze services een alert sturen naar de eigenaar door middel van bijvoorbeeld een email.

**Authentieke en gezaghebbende bronnen**

https://uptimerobot.com/about

https://devcentral.f5.com/articles/back-to-basics-health-monitors-and-load-balancing

## Zeer kritische reflectie 😇 ##

De methode "HandleIncomingRequest" van de "LoadBalancer" is in mijn ogen te ingewikkeld geworden. Zo bevat het logica die eigenlijk verplaatst zou moeten worden naar de specifieke persistence instantie. Ik zet bijvoorbeeld een cookie waarin de webserver id staat en voeg een sessie toe in de session table. Dit komt omdat het mij in eerste instantie slim leek om verschillende implementaties van session persistence te bouwen die een web server terug geven. De load balancer zou dan alleen vragen om een server aan de session persistence implementatie en vervolgens deze server gebruiken om data naar te sturen. In de praktijk bleek dat deze beslissing niet handig was omdat de request en/of response nog eerst aangepast moet worden voordat deze verstuurd kan worden. Alleen een server terug geven was dus niet voldoende. Het was een beter idee geweest om een session persistence implementatie verantwoordelijk te maken om data te versturen, in plaats van dat de load balancer nu deze verantwoordelijk heeft. Hierdoor zou de load balancer minder verantwoordelijk krijgen.